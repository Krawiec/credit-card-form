import * as React from 'react';
import { Card } from './card/card';
import CardExpire from './form/card-expire-input';
import CardInput from './form/card-input';
import { InputType } from '../utils/input-types';

function isNumber(value: string | number): boolean {
  return value != null && !isNaN(Number(value.toString()));
}

type ContainerProps = {
  cardOwner: string;
  cardNumber: string;
  expireMonth: string;
  expireYear: string;
  ccv: string;
  onCardNumberChange: (value: string) => void;
  onCardOwnerChange: (value: string) => void;
  onExpireMonthChange: (value: string) => void;
  onExpireYearChange: (value: string) => void;
  onCcvNumberChange: (value: string) => void;
  flippCard: (toogle: boolean) => void;
};

const Container = ({
  cardNumber,
  cardOwner,
  expireMonth,
  expireYear,
  ccv,
  onCardNumberChange,
  onCardOwnerChange,
  onCcvNumberChange,
  onExpireMonthChange,
  onExpireYearChange,
  flippCard
}: ContainerProps) => {
  return (
    <div className="app">
      <Card
        cardOwner={cardOwner}
        cardNumber={cardNumber}
        expireMonth={expireMonth}
        expireYear={expireYear}
        ccv={ccv}
      />
      <div className="form-container">
        <CardInput
          inputValue={cardNumber}
          onChange={onCardNumberChange}
          inputType={InputType.CardNumber}
          placeholder="Card Number"
          validate={(value) => value.length <= 16 && isNumber(value)}
        />
        <CardInput
          inputValue={cardOwner}
          onChange={onCardOwnerChange}
          inputType={InputType.CardOwner}
          placeholder="Card Owner"
        />
        <div className="expires-date">
          <CardExpire
            placeholder="Select Year"
            value={expireMonth}
            onSelect={onExpireMonthChange}
            inputType={InputType.ExpireDate}
            className="select--flex-grow-one"
            items={months}
          />
          <CardExpire
            placeholder="Select Year"
            value={expireYear}
            onSelect={onExpireYearChange}
            inputType={InputType.ExpireDate}
            className="select--flex-grow-one"
            items={years}
          />
        </div>
        <CardInput
          inputValue={ccv}
          onChange={onCcvNumberChange}
          inputType={InputType.CCV}
          additionalToggleAction={flippCard}
          placeholder="CCV"
          validate={(value) => value.length <= 3 && isNumber(value)}
        />
      </div>
    </div>
  );
};

const months = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
const years = [
  '2010',
  '2011',
  '2012',
  '2013',
  '2014',
  '2015',
  '2016',
  '2017',
  '2018',
  '2019',
  '2020',
  '2021'
];
export default Container;
