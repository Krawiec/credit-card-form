import * as React from 'react';
import { classes } from 'typestyle';
import { useInputFocusContext } from '../../contexts/input-focus';
import { InputType } from '../../utils/input-types';

type ExpiresProps = {
  month: string;
  year: string;
};

export const Expires = ({
  month,
  year
}: ExpiresProps) => {
  const {focusedInput} = useInputFocusContext();
  const focused = focusedInput === InputType.ExpireDate;
  return (
    <div
      className={classes(
        'card-expires',
        focused && 'card-expires--focused'
      )}
    >
      <div className="card-expires__placeholder">Expires:</div>
      <div className="card-expired__date">
        <div className="card-expires__item card-expires__month">
          {month !== '' ? month : 'MM'}
        </div>
        <div className="card-expires__item card-expires__back-slash">/</div>
        <div className="card-expires__item card-expires__year">
          {year !== '' ? year : 'YYYY'}
        </div>
      </div>
    </div>
  );
};
