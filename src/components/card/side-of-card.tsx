import * as React from 'react';
import chip from '../../images/chip.png';
import backgroundCard from '../../images/6.jpeg';
import { classes } from 'typestyle';
import { Logo } from './logo';
import { CardType } from './card-type';
import { useCardFlippedContext } from '../../contexts/card-flipp';

type SideOfCardProps = {
  side: 'back' | 'front';
  showCardLogo?: boolean;
  children?: React.ReactNode;
};

export const SideOfCard = ({
  side,
  showCardLogo,
  children
}: SideOfCardProps) => {
  const { flipped } = useCardFlippedContext();
  return (
    <CardContainer flipped={flipped} side={side}>
      <div className="card__top">
        <Chip side={side} />
        {showCardLogo && (
          <div
            className={classes(
              'top-logo',
              `top-logo--${CardType.Visa.toString().toLowerCase()}`
            )}
          >
            <Logo cardType={CardType.Visa} />
          </div>
        )}
      </div>
      {children}
    </CardContainer>
  );
};

type CardContainerType = {
  flipped: boolean;
  side: 'back' | 'front';
  children: React.ReactNode;
};

const CardContainer = ({ flipped, side, children }: CardContainerType) => {
  return (
    <div
      className={classes(`card__${side}`, flipped && `card__${side}--flipped`)}
      style={{
        backgroundImage: `url(${backgroundCard})`
      }}
    >
      {children}
    </div>
  );
};

type ChipProps = {
  side: 'back' | 'front';
};

const Chip = ({ side }: ChipProps) => {
  return side === 'front' ? (
    <div
      className="card__chip"
      style={{
        backgroundImage: `url(${chip})`
      }}
    />
  ) : (
    <div />
  );
};
