import * as React from 'react';
import { classes } from 'typestyle';
import { useInputFocusContext } from '../../contexts/input-focus';
import { InputType } from '../../utils/input-types';

type CcvProps = {
  ccvNumber: string;
};

export const CCV = ({ ccvNumber }: CcvProps) => {
  const { focusedInput } = useInputFocusContext();
  const focused = focusedInput === InputType.CCV;
  return (
    <div className={classes('ccv', focused && 'ccv--focused')}>
      <div className="ccv__number">{ccvNumber}</div>
    </div>
  );
};
