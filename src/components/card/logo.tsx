import * as React from 'react';
import { CardType } from './card-type';
import { logos } from '../../utils/logos';

type LogoProps = {
  cardType: CardType;
};
export const Logo = ({ cardType }: LogoProps) => {
  return (
      <div
        className="card__logo"
        style={{
          backgroundImage: `url(${logos[cardType]})`
        }}
      />
  );
};
