import * as React from 'react';
import { SideOfCard } from './side-of-card';
import { CardNumber } from './card-number';
import { CardOwner } from './card-owner';
import { classes } from 'typestyle';
import { Expires } from './expires';
import { Logo } from './logo';
import { CardType } from './card-type';
import { CCV } from './ccv';

type CardContainerProps = {
  cardNumber: string;
  cardOwner: string;
  expireMonth: string;
  expireYear: string;
  ccv: string;
};

export const Card = ({
  cardNumber,
  cardOwner,
  expireMonth,
  expireYear,
  ccv,
}: CardContainerProps) => {
  return (
    <div className="card">
      <SideOfCard side="front" showCardLogo={true}>
        <div className={classes('card__content card__content--center')}>
          <CardNumber
            cardNumber={cardNumber}
          />
        </div>
        <div className={classes('card__content card__content--bottom-left')}>
          <CardOwner
            cardOwner={cardOwner}
          />
        </div>
        <div className={classes('card__content card__content--bottom-right')}>
          <Expires
            month={expireMonth}
            year={expireYear}
          />
        </div>
      </SideOfCard>
      <SideOfCard side="back">
        <div className={classes('card__content card__content--center')}>
          <CCV ccvNumber={ccv}/>
        </div>
        <div className={classes('card-back__logo')}>
          <Logo cardType={CardType.Visa} />
        </div>
      </SideOfCard>
    </div>
  );
};
