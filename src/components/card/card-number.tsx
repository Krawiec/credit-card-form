import * as React from 'react';
import { classes } from 'typestyle';
import { useInputFocusContext } from '../../contexts/input-focus';
import { InputType } from '../../utils/input-types';
import { CardLetter } from './card-letter';

type CardNumberProps = {
  cardNumber: string;
};

const template = '################';

const replaceTemplate = (currentCardNumbers: string) => {
  let output = template;
  [...currentCardNumbers].forEach((number: string, index: number) => {
    let character =
      (index >= 0 && index < 4) || (index >= 12 && index < 16) ? number : '*';
    output = output.substr(0, index) + character + output.substr(index + 1);
  });

  return output;
};

export const CardNumber = ({ cardNumber }: CardNumberProps) => {
  const { focusedInput } = useInputFocusContext();
  const focused = focusedInput === InputType.CardNumber;
  const [currentCardNumber, setCurrentCardNumber] = React.useState(template);
  const [numberUnderChanging, setNumberUnderChanging] = React.useState(-1);

  React.useEffect(() => {
    setCurrentCardNumber(replaceTemplate(cardNumber));
    setNumberUnderChanging(cardNumber.length - 1);
  }, [cardNumber]);

  return (
    <div className={classes('card-number', focused && 'card-number--focused')}>
      {[...currentCardNumber].map((number: string, index: number) => (
        <CardLetter
          letter={number}
          classes={classes(
            'card-number__item',
            (index + 1) % 4 === 0 && 'card-number__item--break',
            index === numberUnderChanging && 'card-number__item--hide'
          )}
        />
      ))}
    </div>
  );
};
