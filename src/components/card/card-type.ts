export enum CardType {
    Visa = 'Visa',
    Amex = 'Amex',
    Discover = 'Discover',
    MasterCard = 'MasterCard',
    DinnersClub = 'DinnersClub',
    Troy = 'Troy',
    UnionPay = 'UnionPay'
}