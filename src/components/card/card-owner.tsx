import * as React from 'react';
import { classes } from 'typestyle';
import { useInputFocusContext } from '../../contexts/input-focus';
import { InputType } from '../../utils/input-types';
import { CardLetter } from './card-letter';

type CardOwnerProps = {
  cardOwner: string;
};

export const CardOwner = ({ cardOwner }: CardOwnerProps) => {
  const { focusedInput } = useInputFocusContext();
  const focused = focusedInput === InputType.CardOwner;
  const [numberUnderChanging, setNumberUnderChanging] = React.useState(-1);

  React.useEffect(() => {
    setNumberUnderChanging(cardOwner.length - 1);
  }, [cardOwner]);
  return (
    <div className={classes('card-owner', focused && 'card-owner--focused')}>
      <div className="card-owner__placeholder">Card Owner:</div>
      <div className="card-owner__name-container">
        {[...cardOwner].map((letter: string, index: number) => (
          <CardLetter
            letter={letter}
            classes={classes(
              'card-owner__item',
              `card-owner__${letter !== ' ' ? 'item' : 'break'}`,
              index === numberUnderChanging && 'card-owner__item--hide'
            )}
          />
        ))}
      </div>
    </div>
  );
};
