import * as React from 'react';

type CardLetterProps = {
  letter: string;
  classes: string;
};

export const CardLetter = ({ letter, classes }: CardLetterProps) => {
  return <div className={classes}>{letter}</div>;
};
