import * as React from 'react';
import { useCardFlippedContext } from '../contexts/card-flipp';
import Container from './container';

export const CardInputsManager = () => {
  const { setFlipped } = useCardFlippedContext();
  const [cardNumber, setCardNumber] = React.useState('');

  const [cardOwner, setCardOwner] = React.useState('');

  const [expireMonth, setExpireMonth] = React.useState('');

  const [expireYear, setExpireYear] = React.useState('');

  const [ccv, setCcv] = React.useState('');

  return (
    <Container
      cardNumber={cardNumber}
      cardOwner={cardOwner}
      ccv={ccv}
      expireMonth={expireMonth}
      expireYear={expireYear}
      onCardNumberChange={setCardNumber}
      onCardOwnerChange={setCardOwner}
      onCcvNumberChange={setCcv}
      onExpireMonthChange={setExpireMonth}
      onExpireYearChange={setExpireYear}
      flippCard={setFlipped}
    />
  );
};
