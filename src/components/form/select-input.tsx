import * as React from 'react';
import { classes } from 'typestyle';

type SelectInputProps = {
  value: string;
  items: string[];
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void;
  placeholder: string;
  renderItem: (item: string, key: number) => JSX.Element;
};

export const SelectInput = ({
  value,
  items,
  onChange,
  placeholder,
  renderItem
}: SelectInputProps) => {
  return (
    <div className={classes('input', 'select')}>
      <label className="input__label">{placeholder}</label>
      <select value={value} onChange={onChange} className="input__input">
        {items.map((item: string, key: number) => renderItem(item, key))}
      </select>
    </div>
  );
};
