import * as React from 'react';
import { SelectInput } from './select-input';
import { withToggleFocusBlur } from './withToggleFocusBlur';

type CardExpireProps = {
  value: string;
  onSelect: (month: string) => void;
  placeholder: string;
  items: string[];
};

const CardExpire = ({
  value,
  onSelect,
  items,
  placeholder
}: CardExpireProps) => {
  const onSelectItem = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const { currentTarget: value } = event;
    onSelect(value.value);
  };

  return (
    <SelectInput
      placeholder={placeholder}
      value={value}
      items={items}
      onChange={onSelectItem}
      renderItem={(item, key) => (
        <option value={item} key={key} className='select__item'>
          {item}
        </option>
      )}
    />
  );
};

export default withToggleFocusBlur(CardExpire);
