import * as React from 'react';

type WithValidationProps = {
  validate?: (value: string) => boolean;
  onChange: (value: string) => void;
};

export const withValidation = <P extends any>(
  Component: React.ComponentType<P & WithValidationProps>
) => {
  const WithValidation: React.SFC<P & WithValidationProps> = (
    props: P & WithValidationProps
  ) => {
    const { validate, onChange } = props;
    const onChangeWithValidation = (value: string) => {
      validate ? validate(value) && onChange(value) : onChange(value);
    };
    return <Component {...props} onChange={onChangeWithValidation} />;
  };

  return WithValidation;
};
