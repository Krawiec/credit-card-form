import * as React from 'react';
import useOnFocusOnBlur from '../../utils/useOnFocusOnBlur';
import { useInputFocusContext } from '../../contexts/input-focus';
import { InputType } from '../../utils/input-types';

type WithToggleFocusBlurProps = {
  inputType: InputType;
  className?: string;
  additionalToggleAction?: (toggle: boolean) => void;
};

export const withToggleFocusBlur = <P extends any>(
  Component: React.ComponentType<P>
) => {
  const WithToggleFocusBlur: React.SFC<P & WithToggleFocusBlurProps> = (
    props: P & WithToggleFocusBlurProps
  ) => {
    const {
      className,
      inputType,
      additionalToggleAction
    } = props as WithToggleFocusBlurProps;
    const { setFocusedInput } = useInputFocusContext();
    const onFocus = () => {
      additionalToggleAction && additionalToggleAction(true);
      setFocusedInput(inputType);
    };
    const onBlur = () => {
      additionalToggleAction && additionalToggleAction(false);
    };

    const refElement = useOnFocusOnBlur(onFocus, onBlur);

    return (
      <div
        ref={refElement}
        onFocus={onFocus}
        onBlur={onBlur}
        className={className}
      >
        <Component {...props} />
      </div>
    );
  };

  return WithToggleFocusBlur;
};
