import * as React from 'react';
import { Input } from './input';
import { withToggleFocusBlur } from './withToggleFocusBlur';
import { withValidation } from './withValidation';

type CardInputProps = {
  inputValue: string;
  onChange: (value: string) => void;
  placeholder: string;
};

const CardInput = ({ inputValue, onChange, placeholder }: CardInputProps) => {
  const onInputChange = (value: string) => {
    onChange(value);
  };
  return (
    <Input
      value={inputValue}
      onChange={onInputChange}
      placeholder={placeholder}
    />
  );
};

export default withToggleFocusBlur(withValidation(CardInput));
