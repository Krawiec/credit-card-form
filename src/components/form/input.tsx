import * as React from 'react';

type InputProps = {
  placeholder: string;
  value: string;
  onChange: (value: string) => void;
};

export const Input = ({ value, placeholder, onChange }: InputProps) => {
  const change = (event: React.ChangeEvent<HTMLInputElement>) => {
    const {
      currentTarget: { value }
    } = event;
    onChange(value);
  };
  return (
    <div className='input'>
      <label className='input__label'>{placeholder}</label>
      <input  className='input__input' type="text" value={value} onChange={change} />
    </div>
  );
};
