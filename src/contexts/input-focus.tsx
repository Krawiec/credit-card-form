import * as React from 'react';
import { InputType } from '../utils/input-types';

type InputFocusContextProps = {
  focusedInput: InputType;
  setFocusedInput: (inputType: InputType) => void;
};

const InputFocusContext = React.createContext<InputFocusContextProps>({
  focusedInput: InputType.None,
  setFocusedInput: inputType => console.log(inputType)
});

const useInputFocusContext = () => React.useContext(InputFocusContext);

type Props = {
  children: React.ReactNode;
};

const InputFocusContextProvider = ({ children }: Props) => {
  const [focusedInput, setFocusedInput] = React.useState(InputType.None);
  return (
    <InputFocusContext.Provider
      value={{
        focusedInput,
        setFocusedInput
      }}
    >
      {children}
    </InputFocusContext.Provider>
  );
};

export { useInputFocusContext, InputFocusContextProvider };
  