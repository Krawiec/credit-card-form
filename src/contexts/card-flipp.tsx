import * as React from 'react';

type CardFlippContextProps = {
  flipped: boolean;
  setFlipped: (flipped: boolean) => void;
};

const CardFlippContext = React.createContext<CardFlippContextProps>({
  flipped: false,
  setFlipped: flipped => {
    console.log(flipped);
  }
});

const useCardFlippedContext = () => React.useContext(CardFlippContext);

type Props = {
  children: React.ReactNode;
};

const CardFlipContextProvider = ({ children }: Props) => {
  const [flipped, setFlipped] = React.useState(false);
  return (
    <CardFlippContext.Provider value={{ flipped, setFlipped }}>
      {children}
    </CardFlippContext.Provider>
  );
};

export {
    useCardFlippedContext,
    CardFlipContextProvider
}