import { EnumDictionary } from './enum-dictionary';
import { CardType } from '../components/card/card-type';
import visa from '../images/visa.png';
import amex from '../images/amex.png';
import dinersclub from '../images/dinersclub.png';
import discover from '../images/discover.png';
import mastercard from '../images/mastercard.png';
import troy from '../images/troy.png';
import unionpay from '../images/unionpay.png';

export const logos: EnumDictionary<CardType, any> = {
  [CardType.Amex]: amex,
  [CardType.DinnersClub]: dinersclub,
  [CardType.Discover]: discover,
  [CardType.MasterCard]: mastercard,
  [CardType.Troy]: troy,
  [CardType.UnionPay]: unionpay,
  [CardType.Visa]: visa
};
