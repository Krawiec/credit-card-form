export enum InputType {
    CardNumber = 'CardNumber',
    CardOwner = 'CardOwner',
    ExpireDate = 'ExpireDate',
    CCV = 'CCV',
    None = 'None'
}