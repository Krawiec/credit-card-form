import { useEffect, useRef, RefObject } from 'react';

const useOnFocusOnBlur = (
  onFocus: (e: Event) => void,
  onBlur: (e: Event) => void
): RefObject<any> => {
  const ref = useRef<HTMLElement>(null);

  const handleClick: EventListener = event => {
    if (ref.current.contains(event.target as any)) {
      onFocus(event);
    } else {
      onBlur(event);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClick, true);
    return () => {
      document.removeEventListener('click', handleClick, true);
    };
  }, []);

  return ref;
};

export default useOnFocusOnBlur;
