import * as React from 'react';
import * as ReactDOM from 'react-dom';

import './styles/index.scss';
import { CardFlipContextProvider } from './contexts/card-flipp';
import { InputFocusContextProvider } from './contexts/input-focus';
import { CardInputsManager } from './components/card-inputs-manager';

ReactDOM.render(
  <CardFlipContextProvider>
    <InputFocusContextProvider>
      <CardInputsManager />
    </InputFocusContextProvider>
  </CardFlipContextProvider>,
  document.getElementById('root')
);
